Pour indiquer une nouvelle version de l'addon service.libreelec.settings dans LibreELEC.tv/packages/mediacenter/LibreELEC-settings/package.mk :
```
cd ~/service.libreelec.settings
git log -1 | grep commit | cut -c8-14
```